import java.io.BufferedWriter
import java.io.FileWriter

import scala.tools.nsc.Settings
import scala.tools.nsc.interpreter.IMain
import scala.tools.nsc.interpreter.{Results => IR}
import scala.xml.XML

class NCC(val filePath: String, heisei: Int, ver1: String, ver2: String, interpreter: IMain = null) {
  private val ENCODING_UTF8 = "UTF-8"

  val rawXml = removeCharRefs(io.Source.fromFile(filePath, ENCODING_UTF8).mkString)
  val xml = XML.loadString(rawXml)
  val targetYearData = parseForXtx(heisei, ver1, ver2)

  def hasData = targetYearData.size > 0
  def userName = targetYearData.get("氏名名称").get
  def xtxFileName = s"${targetYearData.get("申告等名").get} ${userName}.xtx"
  def buildXtxString: String = if (targetYearData.size != 0) evalXtxTemplate(getInterpreter) else ""

  def createXtxFile(content: String) {
    if (content.size > 0) {
      println(xtxFileName)
      val bw = new BufferedWriter(new FileWriter(xtxFileName))
      bw.write(content.replaceAll("\r\n", "").replaceAll(">\\s+<", "><").trim)
      bw.close
    } else {
      println(s"${xtxFileName}: No data.")
    }
  }

  private def getInterpreter: IMain = {
    if (interpreter == null) {
      val settings = new Settings
      settings.usejavacp.value = true
      new IMain(settings)
    } else {
      interpreter
    }
  }

  private def removeCharRefs(str: String): String = str.replaceAll("&amp;", "&").replaceAll("&amp;", "&").replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&quot;", "\"").replaceAll("&apos;", "'")

  private def parseForXtx(heisei: Int, ver1: String, ver2: String): Map[String, String] = {
    val sinkokutoukanri = (xml \\ "itemclass" \ "param").filter(_.attribute("name").get.toString == "申告等管理")
    val tyousyo = (sinkokutoukanri \\ "item").filter(_.attribute("name").get.toString.startsWith("平成" + heisei + "年分"))

    if (tyousyo.size != 0) {
      var params = (tyousyo \\ "itemclass" \\ "param").map(elem => elem.attribute("name").get.toString -> elem.text).toMap
      params += "バージョン1" -> ver1
      params += "バージョン2" -> ver2
      params += "ALV00000" -> (tyousyo \\ "ALV00000" \ "kubun_CD")(0).text
      params += "ALT00010" -> (tyousyo \\ "ALT00010" \ "kubun_CD")(0).text
      params += "事業種目" -> (tyousyo \\ "ABG00000")(0).text.trim
      params += "人員" -> (tyousyo \\ "ALJ00020")(0).text
      params += "左のうち、源泉徴収税額のない者" -> (tyousyo \\ "ALJ00030")(0).text
      params += "支払金額" -> (tyousyo \\ "ALJ00040")(0).text
      params += "源泉徴収税額" -> (tyousyo \\ "ALJ00050")(0).text
      params += "署出" -> (tyousyo \\ "ALJ00100")(0).text
      params
    } else {
      Map[String, String]()
    }
  }

  private def evalXtxTemplate(interpreter: IMain): String = {
    val expr = io.Source.fromFile(s"./template/xtx${ver1}_${ver2}.tpl", ENCODING_UTF8).mkString
    interpreter.directBind("data", "Map[String, String]", targetYearData)
    interpreter.interpret(expr) match {
      case IR.Success => interpreter.valueOfTerm("template").get.toString
      case _ => throw new IllegalFormatTemplateException
    }
  }

  class IllegalFormatTemplateException extends Exception
}
