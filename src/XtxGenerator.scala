import java.io.File
import scala.Array.canBuildFrom

object XtxGenerator extends App {
  val dir = """*** file path ***"""
  val heisei = 25
  val ver1 = "12.0.0"
  val ver2 = "5.0"

  val fileNameList = new File(dir).list.filter(_.endsWith(".ncc")).map(fileName => dir + fileName)
  fileNameList.foreach { fileName =>
    val ncc = new NCC(fileName, heisei, ver1, ver2)
    if (ncc.hasData) {
      ncc.createXtxFile(ncc.buildXtxString)
    } else {
      println(s"${fileName}: no data.")
    }
  }
}