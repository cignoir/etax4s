val template = s"""
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<DATA id="DATA" xmlns="http://xml.e-tax.nta.go.jp/XSD/hoteishiryo" xmlns:gen="http://xml.e-tax.nta.go.jp/XSD/general" xmlns:kyo="http://xml.e-tax.nta.go.jp/XSD/kyotsu" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<${data.get("手続きID").get} VR="${data.get("バージョン1").get}" id="${data.get("手続きID").get}">
  <CATALOG id="CATALOG">
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      <rdf:description id="REPORT">
        <SEND_DATA/>
        <IT_SEC>
          <rdf:description about="#IT"/>
        </IT_SEC>
        <FORM_SEC>
          <rdf:Seq>
            <rdf:li>
              <rdf:description about="#${data.get("様式ID").get}-1"/>
            </rdf:li>
          </rdf:Seq>
        </FORM_SEC>
        <TENPU_SEC/>
        <XBRL_SEC/>
        <XBRL2_1_SEC/>
        <SOFUSHO_SEC/>
      </rdf:description>
    </rdf:RDF>
  </CATALOG>
    <CONTENTS id="CONTENTS">
      <IT VR="1.2" id="IT">
        <ZEIMUSHO ID="ZEIMUSHO">
          <gen:zeimusho_CD>${data.get("提出先税務署番号").get}</gen:zeimusho_CD>
          <gen:zeimusho_NM>${data.get("提出先税務署").get}</gen:zeimusho_NM>
        </ZEIMUSHO>
        <NOZEISHA_ID ID="NOZEISHA_ID">${data.get("利用者識別番号").get}</NOZEISHA_ID>
        <NOZEISHA_NM_KN ID="NOZEISHA_NM_KN">${data.get("氏名名称読み").get}</NOZEISHA_NM_KN>
        <NOZEISHA_NM ID="NOZEISHA_NM">${data.get("氏名名称").get}</NOZEISHA_NM>
        <NOZEISHA_ZIP ID="NOZEISHA_ZIP">
          <gen:zip1>${data.get("納税者所在地郵便番号上3桁").get}</gen:zip1>
          <gen:zip2>${data.get("納税者所在地郵便番号下4桁").get}</gen:zip2>
        </NOZEISHA_ZIP>
        <NOZEISHA_ADR ID="NOZEISHA_ADR">${data.get("納税者所在地").get}</NOZEISHA_ADR>
        <NOZEISHA_TEL ID="NOZEISHA_TEL">
          <gen:tel1>${data.get("納税者所在地電話番号市外局番").get}</gen:tel1>
          <gen:tel2>${data.get("納税者所在地電話番号市内局番").get}</gen:tel2>
          <gen:tel3>${data.get("納税者所在地電話番号加入者番号").get}</gen:tel3>
        </NOZEISHA_TEL>
        <SEIBETSU ID="SEIBETSU">
          <kubun_CD>${data.get("性別").get}</kubun_CD>
          <kubun_NM>${if(data.get("性別").get == "1")"男"else"女"}</kubun_NM>
        </SEIBETSU>
        <BIRTHDAY ID="BIRTHDAY">
          <gen:era>${data.get("生年月日年号").get}</gen:era>
          <gen:yy>${data.get("生年月日年").get}</gen:yy>
          <gen:mm>${data.get("生年月日月").get}</gen:mm>
          <gen:dd>${data.get("生年月日日").get}</gen:dd>
        </BIRTHDAY>
        <SHOKUGYO ID="SHOKUGYO">${data.get("職業").get}</SHOKUGYO>
        <DAIRI_ID ID="DAIRI_ID">${data.get("代理人等利用者識別番号").get}</DAIRI_ID>
        <DAIRI_NM ID="DAIRI_NM">${data.get("代理人等氏名").get}</DAIRI_NM>
        <DAIRI_ZIP ID="DAIRI_ZIP">
          <gen:zip1>${data.get("代理人等郵便番号上3桁").get}</gen:zip1>
          <gen:zip2>${data.get("代理人等郵便番号下4桁").get}</gen:zip2>
        </DAIRI_ZIP>
        <DAIRI_ADR ID="DAIRI_ADR">${data.get("代理人等住所").get}</DAIRI_ADR>
        <DAIRI_TEL ID="DAIRI_TEL">
          <gen:tel1>${data.get("代理人等電話番号市外局番").get}</gen:tel1>
          <gen:tel2>${data.get("代理人等電話番号市内局番").get}</gen:tel2>
          <gen:tel3>${data.get("代理人等電話番号加入者番号").get}</gen:tel3>
        </DAIRI_TEL>
        <TETSUZUKI ID="TETSUZUKI">
          <procedure_CD>${data.get("手続きID").get}</procedure_CD>
          <procedure_NM>${data.get("手続き名").get}</procedure_NM>
        </TETSUZUKI>
        <NENBUN ID="NENBUN">
          <gen:era>${data.get("年分年号").get}</gen:era>
          <gen:yy>${data.get("年分年").get}</gen:yy>
        </NENBUN>
      </IT>
      <${data.get("様式ID").get} VR="${data.get("バージョン2").get}" id="${data.get("様式ID").get}-1" page="1" sakuseiDay="${data.get("作成日時").get.split(" ")(0)}" sakuseiNM="${data.get("作成者名").get}" softNM="${data.get("作成ソフト名").get}">
        <ABC00000 IDREF="NENBUN"/>
        <ALV00000>
          <kubun_CD>${data.get("ALV00000").get}</kubun_CD>
        </ALV00000>
        <ABE00000 IDREF="ZEIMUSHO"/>
        <ABF00000>
          <ABF00010 IDREF="NOZEISHA_ADR"/>
          <ABF00020>
            <ABF00030 IDREF="NOZEISHA_NM_KN"/>
            <ABF00040 IDREF="NOZEISHA_NM"/>
          </ABF00020>
          <ABF00090 IDREF="NOZEISHA_TEL"/>
          <ABG00000>${data.get("事業種目").get}</ABG00000>
        </ABF00000>
        <ALT00000>
          <ALT00010>
            <kubun_CD>${data.get("ALT00010").get}</kubun_CD>
          </ALT00010>
        </ALT00000>
        <ALJ00000>
          <ALJ00010>
            <ALJ00020>${data.get("人員").get}</ALJ00020>
            <ALJ00030>${data.get("左のうち、源泉徴収税額のない者").get}</ALJ00030>
            <ALJ00040>${data.get("支払金額").get}</ALJ00040>
            <ALJ00050>${data.get("源泉徴収税額").get}</ALJ00050>
          </ALJ00010>
          <ALJ00090>
            <ALJ00100>${data.get("署出").get}</ALJ00100>
          </ALJ00090>
        </ALJ00000>
        <ALR00000>
          <ALR00010 IDREF="DAIRI_NM"/>
          <ALR00020 IDREF="DAIRI_TEL"/>
        </ALR00000>
      </${data.get("様式ID").get}>
    </CONTENTS>
  </${data.get("手続きID").get}>
</DATA>
"""